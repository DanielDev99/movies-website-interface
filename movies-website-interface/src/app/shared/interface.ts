export interface movie
{
    id:number;
    title:string;
    launchDate:string;
    actors:actor[];
}


export interface actor
{
    id:number;
    name:string;
    dateOfBirth:string;
}