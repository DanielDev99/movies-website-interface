import { Component, OnInit } from '@angular/core';
import {actor} from '../shared/interface';

@Component({
    selector: 'movie',
    templateUrl: './movie.component.html'
})
export class movieComponent implements OnInit
{

    id:number;
    title:string;
    description:string;
    launchDate:string;
    actors:actor[];

    constructor()
    {
        this.id = 1;
        this.title = "dummy data title";
        this.description = "dummy description";
        this.launchDate = "dummy launch date";
        this.actors = 
        [
            {
                id:1,
                name:"Daniel",
                "dateOfBirth":"18-05-1999"
            },
            {
                id:2,
                name:"Alexandru",
                dateOfBirth:"18-06-1997"
            }
        ]
    }


    ngOnInit()
    {
    }
}