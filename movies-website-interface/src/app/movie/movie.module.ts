import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {movieComponent} from './movie.component';


@NgModule({
    imports:      [ CommonModule],
    declarations: [movieComponent],
    exports: [ movieComponent]
  })
  export class movieModule { }